<h2>Users</h2>
 
<!-- link to add new users page -->
<div class='upper-right-opt'>
    <?php echo $this->Html->link( '+ New Users', array( 'action' => 'add' ) ); ?>
</div>
 
<table style='padding:5px;'>
    <!-- table heading -->
    <tr style='background-color:#fff;'>
        <th>ID</th>
        <th>name</th>
        <th>email</th>
        <th>phone</th>
        <th>birthdate</th>
    </tr>
     
<?php
 
     
    //loop to show all retrieved records
    foreach( $users as $user ){
     
        echo "<tr>";
            echo "<td>{$user['id']}</td>";
            echo "<td>{$user['name']}</td>";
            echo "<td>{$user['email']}</td>";
            echo "<td>{$user['phone']}</td>";
            echo "<td>{$user['birthdate']}</td>";
             
            //here are the links to edit and delete actions
            echo "<td class='actions'>";
                echo $this->Html->link( 'Edit', array('action' => 'edit', $user['id']) );
                 
                //in cakephp 2.0, we won't use get request for deleting records
                //we use post request (for security purposes)
                echo $this->Form->postLink( '  Delete', array(
                        'action' => 'delete', 
                        $user['id']), array(
                            'confirm'=>'Are you sure you want to delete that user?' ) );
            echo "</td>";
        echo "</tr>";
   
    }

    
?>
  
</table>

<?php echo $this->Form->postLink( 'Login', array(
        'action' => 'Login') );?>