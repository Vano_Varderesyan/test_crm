<h2>Add New User</h2>
 
<!-- link to add new users page -->
<div class='upper-right-opt'>
    <?php echo $this->Html->link( 'List Users', array( 'action' => 'index' ) ); ?>
</div>
 
<?php 
//this is our add form, name the fields same as database column names
echo $this->Form->create('Users');
 
    // echo $this->Form->input('publisher_id');
    echo $this->Form->input('name');
    echo $this->Form->input('email');
    echo $this->Form->input('phone');
    echo $this->Form->input('birthdate');
    // echo $this->Form->input('phone');
    echo $this->Form->input('username');

    echo $this->Form->input('password', array('type'=>'password'));
     
    echo  $this->Form->button(__('Submit')) ;
echo $this->Form->end();
?>