<h2>Edit User</h2>
 
<!-- link to add new users page -->
<div class='upper-right-opt'>
    <?php echo $this->Html->link( 'List Users', array( 'action' => 'index' ) ); ?>
</div>
 
<?php 
//this is our edit form, name the fields same as database column names
echo $this->Form->create('User');
 
echo $this->Form->input('name',array('value'=>$user['name']));
echo $this->Form->input('email',array('value'=>$user['email']));
echo $this->Form->input('phone',array('value'=>$user['phone']));
echo $this->Form->input('birthdate',array('value'=>$user['birthdate']));
// echo $this->Form->input('phone');
echo $this->Form->input('username',array('value'=>$user['username']));

// echo $this->Form->input('password', array('type'=>'password'));
     
echo  $this->Form->button(__('Submit')) ;
echo $this->Form->end();
?>