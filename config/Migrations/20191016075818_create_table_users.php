<?php

use Phinx\Migration\AbstractMigration;

class CreateTableUsers extends AbstractMigration
{
    public function up()
    {
        $users = $this->table('users');
        $users->addColumn('name','string',['limit'=>100,'null'=>false])
        ->addColumn('email','string',['limit'=>100,'null'=>false])
        ->addColumn('phone','string',['limit'=>100,'null'=>false])
        ->addColumn('birthdate','string',['limit'=>100,'null'=>false])
        ->addColumn('password','string',['limit'=>100,'null'=>false])
        ->addColumn('username','string',['limit'=>100,'null'=>false]);



        $users->save();
    }

    public function down()
    {
        $this->dropTable('users');
    }
}
